function [ m ] = largetipangle(dB0,B1,m)

    dt    = 10^-7;       %0.1 micro second
    gamma = 2*pi*42.577*10^6; % rad * Hz per Tesla
    
    % update m accoridng to equation 6
     m = m + (gamma*dt*[0 dB0 -imag(B1); -dB0 0 real(B1); imag(B1) -real(B1) 0]*m);
end
