function [ mOut ] = complexlargetipangle(dB0,B1,m)

    dt    = 10^-7;       %0.1 micro second
    gamma = 2*pi*42.577*10^6; % rad * Hz per Tesla
    
    mOut  = m; %allocating memory same size as m

    %update m accoring to eqation 8
    mOut = [m(1)-(1i*dt*gamma)*(dB0*m(1)-(B1*m(2))) m(2)+(gamma*dt)*((imag(B1)*real(m(1))-(real(B1)*imag(m(1)))))];

end
