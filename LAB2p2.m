%% Exercise 2.2
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);


%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  


%Question A:
figure
displaysequence(time, rfPulse, gradAmp)
saveas(gcf,'2p2A.png')

%Question C:

tic
for j=1:nPosSteps
    m = [0,1]';

    %update dB0AtPosJ:
    dB0AtPosJ = gradAmp(1) * posZ(j);
    
    % start large tip angle starting from [0,0,1]'
    m = complexlargetipangle(dB0AtPosJ, rfPulse(1), m);
    
    for i=2:nTimeSteps %i starts at 2
        
        %update dB0AtPosJ:
        dB0AtPosJ = gradAmp(i)*posZ(j);
        
        %update udpate m:
        m = complexlargetipangle(dB0AtPosJ, rfPulse(i), m);
    end

    mFinalVect(j,:) = m;
end
toc

%Question D:
load("Lab2p1_ref.mat")

figure()
subplot(3, 1, 1)
plot(posZ, abs(mFinalVect(:,1)), LineWidth=2, Color='b')
hold on
plot(posZ, abs(Lab2p1_ref),'r--', LineWidth=2)
title('Slice Profile, Exc 2.2D')
ylabel('|M_x_y|')
xlabel('z (m)')
legend('Complex sim', 'Pre-sim')
hold off

subplot(3, 1, 2)
plot(posZ, angle(mFinalVect(:,1)), LineWidth=2, Color='b')
hold on
plot(posZ, angle(Lab2p1_ref), 'r--', LineWidth=2)
title('Phase')
ylabel('\angleM_x_y (\circ)')
xlabel('z (m)')
legend('Complex sim', 'Pre-sim')
hold off

subplot(3, 1, 3)
plot(posZ, abs(mFinalVect(:,2)), LineWidth=2, Color='b')
title('Longitudinal Magnitization')
ylabel('M_z')
xlabel('z (m')
legend('Complex Sim')

saveas(gcf,'2p2D.png')

%Question E:
%Elapsed time is 2.757245 seconds.
%This code is slower because MatLab assumes all numbers involved are
%complex.